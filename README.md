#Project Name#
CryptoTrading

#Function List#

CryptoTrading.Program:
Az alapvető CRUD műveletek elvégzésre alkalmas, tehát képesek vagyunk:
	Új kriptovalutát/ felhasználót az adatbázishoz hozzá adni
	Kikérni az adatbázisól a adatokat
	Frissíteni a kriptovaluta árát
	Törölni az adatbázisből
Ezeken felül további 3 műveletet lehetünk képesek el
	Bitocin vásárlásra
	Bitcoin eladásra
	Dupla vagy semmi játék
	
CRUDApp:
WPF-ben megírva az alapvető CRUD műveletek elvégzésre alkalmas, tehát képesek vagyunk:
	Új kriptovalutát az adatbázishoz hozzá adni
	Kikérni az adatbázisól a adatokat
	Szerkeszteni a kriptovaluta adatait
	Törölni az adatbázisből
	
CryptoTrading.Wpf:
WPF-ben megírva API segítségével az alapvető CRUD műveletek elvégzésre alkalmas, tehát képesek vagyunk:
	Új kriptovalutát az adatbázishoz hozzá adni
	Kikérni az adatbázisól a adatokat
	Szerkeszteni a kriptovaluta adatait
	Törölni az adatbázisből
	
#Comment#
A projekt egy féléves feladatként készült el ahol még nem ismertették részletesen a git használatát, 
mindent a master ágra kellett feltenni ezért inkább egy új repositoryt készítettem el ehhez. 

